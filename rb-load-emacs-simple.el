;; setting the theme
(load-theme 'tsdh-dark)

;; hide the toolbar
(tool-bar-mode -1)

;; hide scrollbar
(toggle-scroll-bar -1)

;; display line numbers
(global-display-line-numbers-mode)

;; auto insert closing bracket
(electric-pair-mode 1)

;; make cursor movement stop in between camelCase words
(global-subword-mode 1)

(progn
  ;; turn on highlight matching brackets when cursor is on one
  (show-paren-mode 1)

  ;; highlight brackets
  (setq show-paren-style 'parenthesis)
  )

(progn
  ;; use variable-width font for some modes
  (defun xah-use-variable-width-font ()
    "Set current buffer to use variable-width font."
    (variable-pitch-mode 1)
    ;; (text-scale-increase 1)
    )
  (add-hook 'emacs-lisp-mode-hook 'xah-use-variable-width-font)
  (add-hook 'js-mode-hook 'xah-use-variable-width-font)
  (add-hook 'css-mode-hook 'xah-use-variable-width-font)
  (add-hook 'html-mode-hook 'xah-use-variable-width-font)
  )

;; UTF-8 as default encoding
(set-language-environment "UTF-8")
(set-default-coding-systems 'utf-8)

(add-to-list 'default-frame-alist '(font . "Mononoki 15"))
